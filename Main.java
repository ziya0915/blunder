public class Main {
	public static void main(String args[]) {
		AllUsers all = new AllUsers();
		
		/**
		 * user collection object calling createNewBlunderUser method
		 * by passing the parameters
		 * @param name
		 * @param age
		 * @param profile
		 * @param hobby
		 */
		
		System.out.println("  ** BLUNDERS USERS LIST **  ");
		System.out.println("   -------------------------");
		all.createNewBlunderUser("ziya" ,  "20" , "Software Engineer" ,   "read");
		all.createNewBlunderUser("sonu" ,  "22" , "Mechanical Engineer" ,   "unread");
		all.createNewBlunderUser("monu" ,  "19" , "civil Engineer" ,   "unread");
		all.createNewBlunderUser("shannu", "24" , "Chemical Engineer" , "read");
}
