public class AllUsers {

		/** 
		 * creating a parent class AllUsers to get all the user details
		 * @author ziya1
		 */
			String name;                  //To represents the name
			int age;                      //To represents the age
			String profile;               //To represents the profile
			String primary_hobby;         //To represent the primary hobby
			
			 /**
			  * To create a constructor with arguments for the User class
			  * @param name
			  * @param age
			  * @param profile
			  * @param hobby
			  */
			
			public  void createNewBLunderUser(String name ,int age ,String profile,String primary_hobby ) {
				this.name             = name;               //using this constructor
				this.age              = age;              //using this constructor
				this.profile          =profile ;          //using this constructor
				this.primary_hobby    =primary_hobby;
				System.out.println(this);

	}	
	        /**
		 	 * override the default toString method of Message 
		 	 * so that message can be printed in human readable format
		 	 */
	 
	      @Override
		 	public String toString() {
		 		String StringToReturn = "";
		 		StringToReturn += "Name         :     "+this.name+             "\n";
		 		StringToReturn += "Age          :     "+this.age+              "\n";
		 		StringToReturn += "Profile      :     "+this.profile+          "\n";
		 		StringToReturn += "Hobby        :     "+this.primary_hobby+    "\n";
		 		return StringToReturn;
		 		}
			
		 }


